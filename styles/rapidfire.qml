<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.14.1-Pi" minScale="100000000" labelsEnabled="0" simplifyAlgorithm="0" readOnly="0" simplifyLocal="1" simplifyMaxScale="1" hasScaleBasedVisibilityFlag="0" maxScale="0" simplifyDrawingHints="1" simplifyDrawingTol="1" styleCategories="AllStyleCategories">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <temporal mode="0" durationUnit="min" durationField="" endExpression="" accumulate="0" startField="" endField="" startExpression="" enabled="0" fixedDuration="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <renderer-v2 forceraster="0" enableorderby="0" symbollevels="0" type="RuleRenderer">
    <rules key="{bd0d2663-b580-4ecb-97c0-098971c6788d}">
      <rule label="Fire" key="{95c9fd6b-13ff-4ccc-b744-38981f23feec}" symbol="0" filter="(&quot;auto_class&quot; = 'Fire' and &quot;manual_class&quot; != 'NoFire' )  or  (&quot;manual_class&quot; = 'Fire')   or  (&quot;auto_class&quot; = 'Fire' and &quot;manual_class&quot; IS NULL )"/>
      <rule label="NoFire" key="{8ab9609e-84cd-4145-a27e-552033b523b2}" symbol="1" filter="&quot;manual_class&quot; = 'NoFire' or (&quot;auto_class&quot; = 'False-positive' and  &quot;manual_class&quot; != 'Fire') or (&quot;auto_class&quot; = 'False-positive' and  &quot;manual_class&quot; IS NULL)" checkstate="0"/>
    </rules>
    <symbols>
      <symbol clip_to_extent="1" name="0" force_rhr="0" type="fill" alpha="1">
        <layer locked="0" pass="0" enabled="1" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="182,0,3,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="214,5,9,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="1" force_rhr="0" type="fill" alpha="1">
        <layer locked="0" pass="0" enabled="1" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="229,182,54,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="255,227,43,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>"fid"</value>
    </property>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory scaleBasedVisibility="0" barWidth="5" rotationOffset="270" minScaleDenominator="0" direction="0" lineSizeScale="3x:0,0,0,0,0,0" scaleDependency="Area" showAxis="1" spacingUnit="MM" diagramOrientation="Up" labelPlacementMethod="XHeight" spacing="5" height="15" sizeScale="3x:0,0,0,0,0,0" minimumSize="0" maxScaleDenominator="1e+8" lineSizeType="MM" backgroundColor="#ffffff" penColor="#000000" width="15" penWidth="0" sizeType="MM" enabled="0" backgroundAlpha="255" spacingUnitScale="3x:0,0,0,0,0,0" opacity="1" penAlpha="255">
      <fontProperties style="" description="Sans Serif,9,-1,5,50,0,0,0,0,0"/>
      <axisSymbol>
        <symbol clip_to_extent="1" name="" force_rhr="0" type="line" alpha="1">
          <layer locked="0" pass="0" enabled="1" class="SimpleLine">
            <prop k="capstyle" v="square"/>
            <prop k="customdash" v="5;2"/>
            <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="customdash_unit" v="MM"/>
            <prop k="draw_inside_polygon" v="0"/>
            <prop k="joinstyle" v="bevel"/>
            <prop k="line_color" v="35,35,35,255"/>
            <prop k="line_style" v="solid"/>
            <prop k="line_width" v="0.26"/>
            <prop k="line_width_unit" v="MM"/>
            <prop k="offset" v="0"/>
            <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="offset_unit" v="MM"/>
            <prop k="ring_filter" v="0"/>
            <prop k="use_custom_dash" v="0"/>
            <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" type="QString" value=""/>
                <Option name="properties"/>
                <Option name="type" type="QString" value="collection"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings linePlacementFlags="18" dist="0" priority="0" placement="1" showAll="1" zIndex="0" obstacle="0">
    <properties>
      <Option type="Map">
        <Option name="name" type="QString" value=""/>
        <Option name="properties"/>
        <Option name="type" type="QString" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration type="Map">
      <Option name="QgsGeometryGapCheck" type="Map">
        <Option name="allowedGapsBuffer" type="double" value="0"/>
        <Option name="allowedGapsEnabled" type="bool" value="false"/>
        <Option name="allowedGapsLayer" type="QString" value=""/>
      </Option>
    </checkConfiguration>
  </geometryOptions>
  <referencedLayers/>
  <referencingLayers/>
  <fieldConfiguration>
    <field name="fid">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="DN">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="previous_reflectance_mean_blue">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="previous_reflectance_min_blue">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="previous_reflectance_max_blue">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="previous_reflectance_mean_green">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="previous_reflectance_min_green">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="previous_reflectance_max_green">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="previous_reflectance_mean_red">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="previous_reflectance_min_red">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="previous_reflectance_max_red">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="previous_reflectance_mean_NIR">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="previous_reflectance_min_NIR">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="previous_reflectance_max_NIR">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="previous_reflectance_mean_IR">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="previous_reflectance_min_IR">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="previous_reflectance_max_IR">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="previous_reflectance_mean_SWIR">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="previous_reflectance_min_SWIR">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="previous_reflectance_max_SWIR">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="current_reflectance_mean_blue">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="current_reflectance_min_blue">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="current_reflectance_max_blue">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="current_reflectance_mean_green">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="current_reflectance_min_green">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="current_reflectance_max_green">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="current_reflectance_mean_red">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="current_reflectance_min_red">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="current_reflectance_max_red">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="current_reflectance_mean_NIR">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="current_reflectance_min_NIR">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="current_reflectance_max_NIR">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="current_reflectance_mean_IR">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="current_reflectance_min_IR">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="current_reflectance_max_IR">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="current_reflectance_mean_SWIR">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="current_reflectance_min_SWIR">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="current_reflectance_max_SWIR">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="markers_count_Band1_blue">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="water_index_mean_blue">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="area">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="seed_area_prop">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="overgrown">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="high_dNBR">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="missed_clouds_prev">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="haze">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="water_related_change">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="auto_class">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="manual_class">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="current_nbr">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="previous_nbr">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="dNBR">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="length">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="area_len">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="haze_index">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="" field="fid"/>
    <alias index="1" name="" field="DN"/>
    <alias index="2" name="" field="previous_reflectance_mean_blue"/>
    <alias index="3" name="" field="previous_reflectance_min_blue"/>
    <alias index="4" name="" field="previous_reflectance_max_blue"/>
    <alias index="5" name="" field="previous_reflectance_mean_green"/>
    <alias index="6" name="" field="previous_reflectance_min_green"/>
    <alias index="7" name="" field="previous_reflectance_max_green"/>
    <alias index="8" name="" field="previous_reflectance_mean_red"/>
    <alias index="9" name="" field="previous_reflectance_min_red"/>
    <alias index="10" name="" field="previous_reflectance_max_red"/>
    <alias index="11" name="" field="previous_reflectance_mean_NIR"/>
    <alias index="12" name="" field="previous_reflectance_min_NIR"/>
    <alias index="13" name="" field="previous_reflectance_max_NIR"/>
    <alias index="14" name="" field="previous_reflectance_mean_IR"/>
    <alias index="15" name="" field="previous_reflectance_min_IR"/>
    <alias index="16" name="" field="previous_reflectance_max_IR"/>
    <alias index="17" name="" field="previous_reflectance_mean_SWIR"/>
    <alias index="18" name="" field="previous_reflectance_min_SWIR"/>
    <alias index="19" name="" field="previous_reflectance_max_SWIR"/>
    <alias index="20" name="" field="current_reflectance_mean_blue"/>
    <alias index="21" name="" field="current_reflectance_min_blue"/>
    <alias index="22" name="" field="current_reflectance_max_blue"/>
    <alias index="23" name="" field="current_reflectance_mean_green"/>
    <alias index="24" name="" field="current_reflectance_min_green"/>
    <alias index="25" name="" field="current_reflectance_max_green"/>
    <alias index="26" name="" field="current_reflectance_mean_red"/>
    <alias index="27" name="" field="current_reflectance_min_red"/>
    <alias index="28" name="" field="current_reflectance_max_red"/>
    <alias index="29" name="" field="current_reflectance_mean_NIR"/>
    <alias index="30" name="" field="current_reflectance_min_NIR"/>
    <alias index="31" name="" field="current_reflectance_max_NIR"/>
    <alias index="32" name="" field="current_reflectance_mean_IR"/>
    <alias index="33" name="" field="current_reflectance_min_IR"/>
    <alias index="34" name="" field="current_reflectance_max_IR"/>
    <alias index="35" name="" field="current_reflectance_mean_SWIR"/>
    <alias index="36" name="" field="current_reflectance_min_SWIR"/>
    <alias index="37" name="" field="current_reflectance_max_SWIR"/>
    <alias index="38" name="" field="markers_count_Band1_blue"/>
    <alias index="39" name="" field="water_index_mean_blue"/>
    <alias index="40" name="" field="area"/>
    <alias index="41" name="" field="seed_area_prop"/>
    <alias index="42" name="" field="overgrown"/>
    <alias index="43" name="" field="high_dNBR"/>
    <alias index="44" name="" field="missed_clouds_prev"/>
    <alias index="45" name="" field="haze"/>
    <alias index="46" name="" field="water_related_change"/>
    <alias index="47" name="" field="auto_class"/>
    <alias index="48" name="" field="manual_class"/>
    <alias index="49" name="" field="current_nbr"/>
    <alias index="50" name="" field="previous_nbr"/>
    <alias index="51" name="" field="dNBR"/>
    <alias index="52" name="" field="length"/>
    <alias index="53" name="" field="area_len"/>
    <alias index="54" name="" field="haze_index"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" applyOnUpdate="0" field="fid"/>
    <default expression="" applyOnUpdate="0" field="DN"/>
    <default expression="" applyOnUpdate="0" field="previous_reflectance_mean_blue"/>
    <default expression="" applyOnUpdate="0" field="previous_reflectance_min_blue"/>
    <default expression="" applyOnUpdate="0" field="previous_reflectance_max_blue"/>
    <default expression="" applyOnUpdate="0" field="previous_reflectance_mean_green"/>
    <default expression="" applyOnUpdate="0" field="previous_reflectance_min_green"/>
    <default expression="" applyOnUpdate="0" field="previous_reflectance_max_green"/>
    <default expression="" applyOnUpdate="0" field="previous_reflectance_mean_red"/>
    <default expression="" applyOnUpdate="0" field="previous_reflectance_min_red"/>
    <default expression="" applyOnUpdate="0" field="previous_reflectance_max_red"/>
    <default expression="" applyOnUpdate="0" field="previous_reflectance_mean_NIR"/>
    <default expression="" applyOnUpdate="0" field="previous_reflectance_min_NIR"/>
    <default expression="" applyOnUpdate="0" field="previous_reflectance_max_NIR"/>
    <default expression="" applyOnUpdate="0" field="previous_reflectance_mean_IR"/>
    <default expression="" applyOnUpdate="0" field="previous_reflectance_min_IR"/>
    <default expression="" applyOnUpdate="0" field="previous_reflectance_max_IR"/>
    <default expression="" applyOnUpdate="0" field="previous_reflectance_mean_SWIR"/>
    <default expression="" applyOnUpdate="0" field="previous_reflectance_min_SWIR"/>
    <default expression="" applyOnUpdate="0" field="previous_reflectance_max_SWIR"/>
    <default expression="" applyOnUpdate="0" field="current_reflectance_mean_blue"/>
    <default expression="" applyOnUpdate="0" field="current_reflectance_min_blue"/>
    <default expression="" applyOnUpdate="0" field="current_reflectance_max_blue"/>
    <default expression="" applyOnUpdate="0" field="current_reflectance_mean_green"/>
    <default expression="" applyOnUpdate="0" field="current_reflectance_min_green"/>
    <default expression="" applyOnUpdate="0" field="current_reflectance_max_green"/>
    <default expression="" applyOnUpdate="0" field="current_reflectance_mean_red"/>
    <default expression="" applyOnUpdate="0" field="current_reflectance_min_red"/>
    <default expression="" applyOnUpdate="0" field="current_reflectance_max_red"/>
    <default expression="" applyOnUpdate="0" field="current_reflectance_mean_NIR"/>
    <default expression="" applyOnUpdate="0" field="current_reflectance_min_NIR"/>
    <default expression="" applyOnUpdate="0" field="current_reflectance_max_NIR"/>
    <default expression="" applyOnUpdate="0" field="current_reflectance_mean_IR"/>
    <default expression="" applyOnUpdate="0" field="current_reflectance_min_IR"/>
    <default expression="" applyOnUpdate="0" field="current_reflectance_max_IR"/>
    <default expression="" applyOnUpdate="0" field="current_reflectance_mean_SWIR"/>
    <default expression="" applyOnUpdate="0" field="current_reflectance_min_SWIR"/>
    <default expression="" applyOnUpdate="0" field="current_reflectance_max_SWIR"/>
    <default expression="" applyOnUpdate="0" field="markers_count_Band1_blue"/>
    <default expression="" applyOnUpdate="0" field="water_index_mean_blue"/>
    <default expression="" applyOnUpdate="0" field="area"/>
    <default expression="" applyOnUpdate="0" field="seed_area_prop"/>
    <default expression="" applyOnUpdate="0" field="overgrown"/>
    <default expression="" applyOnUpdate="0" field="high_dNBR"/>
    <default expression="" applyOnUpdate="0" field="missed_clouds_prev"/>
    <default expression="" applyOnUpdate="0" field="haze"/>
    <default expression="" applyOnUpdate="0" field="water_related_change"/>
    <default expression="" applyOnUpdate="0" field="auto_class"/>
    <default expression="" applyOnUpdate="0" field="manual_class"/>
    <default expression="" applyOnUpdate="0" field="current_nbr"/>
    <default expression="" applyOnUpdate="0" field="previous_nbr"/>
    <default expression="" applyOnUpdate="0" field="dNBR"/>
    <default expression="" applyOnUpdate="0" field="length"/>
    <default expression="" applyOnUpdate="0" field="area_len"/>
    <default expression="" applyOnUpdate="0" field="haze_index"/>
  </defaults>
  <constraints>
    <constraint constraints="3" unique_strength="1" notnull_strength="1" exp_strength="0" field="fid"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="DN"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="previous_reflectance_mean_blue"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="previous_reflectance_min_blue"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="previous_reflectance_max_blue"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="previous_reflectance_mean_green"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="previous_reflectance_min_green"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="previous_reflectance_max_green"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="previous_reflectance_mean_red"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="previous_reflectance_min_red"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="previous_reflectance_max_red"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="previous_reflectance_mean_NIR"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="previous_reflectance_min_NIR"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="previous_reflectance_max_NIR"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="previous_reflectance_mean_IR"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="previous_reflectance_min_IR"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="previous_reflectance_max_IR"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="previous_reflectance_mean_SWIR"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="previous_reflectance_min_SWIR"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="previous_reflectance_max_SWIR"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="current_reflectance_mean_blue"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="current_reflectance_min_blue"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="current_reflectance_max_blue"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="current_reflectance_mean_green"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="current_reflectance_min_green"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="current_reflectance_max_green"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="current_reflectance_mean_red"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="current_reflectance_min_red"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="current_reflectance_max_red"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="current_reflectance_mean_NIR"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="current_reflectance_min_NIR"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="current_reflectance_max_NIR"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="current_reflectance_mean_IR"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="current_reflectance_min_IR"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="current_reflectance_max_IR"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="current_reflectance_mean_SWIR"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="current_reflectance_min_SWIR"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="current_reflectance_max_SWIR"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="markers_count_Band1_blue"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="water_index_mean_blue"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="area"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="seed_area_prop"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="overgrown"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="high_dNBR"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="missed_clouds_prev"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="haze"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="water_related_change"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="auto_class"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="manual_class"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="current_nbr"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="previous_nbr"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="dNBR"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="length"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="area_len"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="haze_index"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="fid"/>
    <constraint desc="" exp="" field="DN"/>
    <constraint desc="" exp="" field="previous_reflectance_mean_blue"/>
    <constraint desc="" exp="" field="previous_reflectance_min_blue"/>
    <constraint desc="" exp="" field="previous_reflectance_max_blue"/>
    <constraint desc="" exp="" field="previous_reflectance_mean_green"/>
    <constraint desc="" exp="" field="previous_reflectance_min_green"/>
    <constraint desc="" exp="" field="previous_reflectance_max_green"/>
    <constraint desc="" exp="" field="previous_reflectance_mean_red"/>
    <constraint desc="" exp="" field="previous_reflectance_min_red"/>
    <constraint desc="" exp="" field="previous_reflectance_max_red"/>
    <constraint desc="" exp="" field="previous_reflectance_mean_NIR"/>
    <constraint desc="" exp="" field="previous_reflectance_min_NIR"/>
    <constraint desc="" exp="" field="previous_reflectance_max_NIR"/>
    <constraint desc="" exp="" field="previous_reflectance_mean_IR"/>
    <constraint desc="" exp="" field="previous_reflectance_min_IR"/>
    <constraint desc="" exp="" field="previous_reflectance_max_IR"/>
    <constraint desc="" exp="" field="previous_reflectance_mean_SWIR"/>
    <constraint desc="" exp="" field="previous_reflectance_min_SWIR"/>
    <constraint desc="" exp="" field="previous_reflectance_max_SWIR"/>
    <constraint desc="" exp="" field="current_reflectance_mean_blue"/>
    <constraint desc="" exp="" field="current_reflectance_min_blue"/>
    <constraint desc="" exp="" field="current_reflectance_max_blue"/>
    <constraint desc="" exp="" field="current_reflectance_mean_green"/>
    <constraint desc="" exp="" field="current_reflectance_min_green"/>
    <constraint desc="" exp="" field="current_reflectance_max_green"/>
    <constraint desc="" exp="" field="current_reflectance_mean_red"/>
    <constraint desc="" exp="" field="current_reflectance_min_red"/>
    <constraint desc="" exp="" field="current_reflectance_max_red"/>
    <constraint desc="" exp="" field="current_reflectance_mean_NIR"/>
    <constraint desc="" exp="" field="current_reflectance_min_NIR"/>
    <constraint desc="" exp="" field="current_reflectance_max_NIR"/>
    <constraint desc="" exp="" field="current_reflectance_mean_IR"/>
    <constraint desc="" exp="" field="current_reflectance_min_IR"/>
    <constraint desc="" exp="" field="current_reflectance_max_IR"/>
    <constraint desc="" exp="" field="current_reflectance_mean_SWIR"/>
    <constraint desc="" exp="" field="current_reflectance_min_SWIR"/>
    <constraint desc="" exp="" field="current_reflectance_max_SWIR"/>
    <constraint desc="" exp="" field="markers_count_Band1_blue"/>
    <constraint desc="" exp="" field="water_index_mean_blue"/>
    <constraint desc="" exp="" field="area"/>
    <constraint desc="" exp="" field="seed_area_prop"/>
    <constraint desc="" exp="" field="overgrown"/>
    <constraint desc="" exp="" field="high_dNBR"/>
    <constraint desc="" exp="" field="missed_clouds_prev"/>
    <constraint desc="" exp="" field="haze"/>
    <constraint desc="" exp="" field="water_related_change"/>
    <constraint desc="" exp="" field="auto_class"/>
    <constraint desc="" exp="" field="manual_class"/>
    <constraint desc="" exp="" field="current_nbr"/>
    <constraint desc="" exp="" field="previous_nbr"/>
    <constraint desc="" exp="" field="dNBR"/>
    <constraint desc="" exp="" field="length"/>
    <constraint desc="" exp="" field="area_len"/>
    <constraint desc="" exp="" field="haze_index"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortExpression="&quot;auto_class&quot;" actionWidgetStyle="dropDown" sortOrder="1">
    <columns>
      <column hidden="0" name="fid" type="field" width="-1"/>
      <column hidden="0" name="DN" type="field" width="-1"/>
      <column hidden="0" name="previous_reflectance_mean_blue" type="field" width="-1"/>
      <column hidden="0" name="previous_reflectance_min_blue" type="field" width="-1"/>
      <column hidden="0" name="previous_reflectance_max_blue" type="field" width="-1"/>
      <column hidden="0" name="previous_reflectance_mean_green" type="field" width="-1"/>
      <column hidden="0" name="previous_reflectance_min_green" type="field" width="-1"/>
      <column hidden="0" name="previous_reflectance_max_green" type="field" width="-1"/>
      <column hidden="0" name="previous_reflectance_mean_red" type="field" width="-1"/>
      <column hidden="0" name="previous_reflectance_min_red" type="field" width="-1"/>
      <column hidden="0" name="previous_reflectance_max_red" type="field" width="-1"/>
      <column hidden="0" name="previous_reflectance_mean_NIR" type="field" width="-1"/>
      <column hidden="0" name="previous_reflectance_min_NIR" type="field" width="-1"/>
      <column hidden="0" name="previous_reflectance_max_NIR" type="field" width="-1"/>
      <column hidden="0" name="previous_reflectance_mean_IR" type="field" width="-1"/>
      <column hidden="0" name="previous_reflectance_min_IR" type="field" width="-1"/>
      <column hidden="0" name="previous_reflectance_max_IR" type="field" width="-1"/>
      <column hidden="0" name="previous_reflectance_mean_SWIR" type="field" width="-1"/>
      <column hidden="0" name="previous_reflectance_min_SWIR" type="field" width="-1"/>
      <column hidden="0" name="previous_reflectance_max_SWIR" type="field" width="-1"/>
      <column hidden="0" name="current_reflectance_mean_blue" type="field" width="-1"/>
      <column hidden="0" name="current_reflectance_min_blue" type="field" width="-1"/>
      <column hidden="0" name="current_reflectance_max_blue" type="field" width="-1"/>
      <column hidden="0" name="current_reflectance_mean_green" type="field" width="-1"/>
      <column hidden="0" name="current_reflectance_min_green" type="field" width="-1"/>
      <column hidden="0" name="current_reflectance_max_green" type="field" width="-1"/>
      <column hidden="0" name="current_reflectance_mean_red" type="field" width="-1"/>
      <column hidden="0" name="current_reflectance_min_red" type="field" width="-1"/>
      <column hidden="0" name="current_reflectance_max_red" type="field" width="-1"/>
      <column hidden="0" name="current_reflectance_mean_NIR" type="field" width="-1"/>
      <column hidden="0" name="current_reflectance_min_NIR" type="field" width="-1"/>
      <column hidden="0" name="current_reflectance_max_NIR" type="field" width="-1"/>
      <column hidden="0" name="current_reflectance_mean_IR" type="field" width="-1"/>
      <column hidden="0" name="current_reflectance_min_IR" type="field" width="-1"/>
      <column hidden="0" name="current_reflectance_max_IR" type="field" width="-1"/>
      <column hidden="0" name="current_reflectance_mean_SWIR" type="field" width="-1"/>
      <column hidden="0" name="current_reflectance_min_SWIR" type="field" width="-1"/>
      <column hidden="0" name="current_reflectance_max_SWIR" type="field" width="-1"/>
      <column hidden="0" name="markers_count_Band1_blue" type="field" width="-1"/>
      <column hidden="0" name="water_index_mean_blue" type="field" width="-1"/>
      <column hidden="0" name="area" type="field" width="-1"/>
      <column hidden="0" name="seed_area_prop" type="field" width="-1"/>
      <column hidden="0" name="overgrown" type="field" width="-1"/>
      <column hidden="0" name="high_dNBR" type="field" width="-1"/>
      <column hidden="0" name="missed_clouds_prev" type="field" width="-1"/>
      <column hidden="0" name="haze" type="field" width="-1"/>
      <column hidden="0" name="water_related_change" type="field" width="-1"/>
      <column hidden="0" name="auto_class" type="field" width="-1"/>
      <column hidden="0" name="manual_class" type="field" width="-1"/>
      <column hidden="0" name="current_nbr" type="field" width="-1"/>
      <column hidden="0" name="previous_nbr" type="field" width="-1"/>
      <column hidden="0" name="dNBR" type="field" width="-1"/>
      <column hidden="0" name="length" type="field" width="-1"/>
      <column hidden="0" name="area_len" type="field" width="-1"/>
      <column hidden="0" name="haze_index" type="field" width="-1"/>
      <column hidden="1" type="actions" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="DN" editable="1"/>
    <field name="area" editable="1"/>
    <field name="area_len" editable="1"/>
    <field name="auto_class" editable="1"/>
    <field name="current_nbr" editable="1"/>
    <field name="current_reflectance_max_IR" editable="1"/>
    <field name="current_reflectance_max_NIR" editable="1"/>
    <field name="current_reflectance_max_SWIR" editable="1"/>
    <field name="current_reflectance_max_blue" editable="1"/>
    <field name="current_reflectance_max_green" editable="1"/>
    <field name="current_reflectance_max_red" editable="1"/>
    <field name="current_reflectance_mean_IR" editable="1"/>
    <field name="current_reflectance_mean_NIR" editable="1"/>
    <field name="current_reflectance_mean_SWIR" editable="1"/>
    <field name="current_reflectance_mean_blue" editable="1"/>
    <field name="current_reflectance_mean_green" editable="1"/>
    <field name="current_reflectance_mean_red" editable="1"/>
    <field name="current_reflectance_min_IR" editable="1"/>
    <field name="current_reflectance_min_NIR" editable="1"/>
    <field name="current_reflectance_min_SWIR" editable="1"/>
    <field name="current_reflectance_min_blue" editable="1"/>
    <field name="current_reflectance_min_green" editable="1"/>
    <field name="current_reflectance_min_red" editable="1"/>
    <field name="dNBR" editable="1"/>
    <field name="fid" editable="1"/>
    <field name="haze" editable="1"/>
    <field name="haze_index" editable="1"/>
    <field name="high_dNBR" editable="1"/>
    <field name="length" editable="1"/>
    <field name="manual_class" editable="1"/>
    <field name="markers_count_Band1_blue" editable="1"/>
    <field name="missed_clouds_prev" editable="1"/>
    <field name="overgrown" editable="1"/>
    <field name="previous_nbr" editable="1"/>
    <field name="previous_reflectance_max_IR" editable="1"/>
    <field name="previous_reflectance_max_NIR" editable="1"/>
    <field name="previous_reflectance_max_SWIR" editable="1"/>
    <field name="previous_reflectance_max_blue" editable="1"/>
    <field name="previous_reflectance_max_green" editable="1"/>
    <field name="previous_reflectance_max_red" editable="1"/>
    <field name="previous_reflectance_mean_IR" editable="1"/>
    <field name="previous_reflectance_mean_NIR" editable="1"/>
    <field name="previous_reflectance_mean_SWIR" editable="1"/>
    <field name="previous_reflectance_mean_blue" editable="1"/>
    <field name="previous_reflectance_mean_green" editable="1"/>
    <field name="previous_reflectance_mean_red" editable="1"/>
    <field name="previous_reflectance_min_IR" editable="1"/>
    <field name="previous_reflectance_min_NIR" editable="1"/>
    <field name="previous_reflectance_min_SWIR" editable="1"/>
    <field name="previous_reflectance_min_blue" editable="1"/>
    <field name="previous_reflectance_min_green" editable="1"/>
    <field name="previous_reflectance_min_red" editable="1"/>
    <field name="seed_area_prop" editable="1"/>
    <field name="water_index_mean_blue" editable="1"/>
    <field name="water_related_change" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="DN"/>
    <field labelOnTop="0" name="area"/>
    <field labelOnTop="0" name="area_len"/>
    <field labelOnTop="0" name="auto_class"/>
    <field labelOnTop="0" name="current_nbr"/>
    <field labelOnTop="0" name="current_reflectance_max_IR"/>
    <field labelOnTop="0" name="current_reflectance_max_NIR"/>
    <field labelOnTop="0" name="current_reflectance_max_SWIR"/>
    <field labelOnTop="0" name="current_reflectance_max_blue"/>
    <field labelOnTop="0" name="current_reflectance_max_green"/>
    <field labelOnTop="0" name="current_reflectance_max_red"/>
    <field labelOnTop="0" name="current_reflectance_mean_IR"/>
    <field labelOnTop="0" name="current_reflectance_mean_NIR"/>
    <field labelOnTop="0" name="current_reflectance_mean_SWIR"/>
    <field labelOnTop="0" name="current_reflectance_mean_blue"/>
    <field labelOnTop="0" name="current_reflectance_mean_green"/>
    <field labelOnTop="0" name="current_reflectance_mean_red"/>
    <field labelOnTop="0" name="current_reflectance_min_IR"/>
    <field labelOnTop="0" name="current_reflectance_min_NIR"/>
    <field labelOnTop="0" name="current_reflectance_min_SWIR"/>
    <field labelOnTop="0" name="current_reflectance_min_blue"/>
    <field labelOnTop="0" name="current_reflectance_min_green"/>
    <field labelOnTop="0" name="current_reflectance_min_red"/>
    <field labelOnTop="0" name="dNBR"/>
    <field labelOnTop="0" name="fid"/>
    <field labelOnTop="0" name="haze"/>
    <field labelOnTop="0" name="haze_index"/>
    <field labelOnTop="0" name="high_dNBR"/>
    <field labelOnTop="0" name="length"/>
    <field labelOnTop="0" name="manual_class"/>
    <field labelOnTop="0" name="markers_count_Band1_blue"/>
    <field labelOnTop="0" name="missed_clouds_prev"/>
    <field labelOnTop="0" name="overgrown"/>
    <field labelOnTop="0" name="previous_nbr"/>
    <field labelOnTop="0" name="previous_reflectance_max_IR"/>
    <field labelOnTop="0" name="previous_reflectance_max_NIR"/>
    <field labelOnTop="0" name="previous_reflectance_max_SWIR"/>
    <field labelOnTop="0" name="previous_reflectance_max_blue"/>
    <field labelOnTop="0" name="previous_reflectance_max_green"/>
    <field labelOnTop="0" name="previous_reflectance_max_red"/>
    <field labelOnTop="0" name="previous_reflectance_mean_IR"/>
    <field labelOnTop="0" name="previous_reflectance_mean_NIR"/>
    <field labelOnTop="0" name="previous_reflectance_mean_SWIR"/>
    <field labelOnTop="0" name="previous_reflectance_mean_blue"/>
    <field labelOnTop="0" name="previous_reflectance_mean_green"/>
    <field labelOnTop="0" name="previous_reflectance_mean_red"/>
    <field labelOnTop="0" name="previous_reflectance_min_IR"/>
    <field labelOnTop="0" name="previous_reflectance_min_NIR"/>
    <field labelOnTop="0" name="previous_reflectance_min_SWIR"/>
    <field labelOnTop="0" name="previous_reflectance_min_blue"/>
    <field labelOnTop="0" name="previous_reflectance_min_green"/>
    <field labelOnTop="0" name="previous_reflectance_min_red"/>
    <field labelOnTop="0" name="seed_area_prop"/>
    <field labelOnTop="0" name="water_index_mean_blue"/>
    <field labelOnTop="0" name="water_related_change"/>
  </labelOnTop>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"fid"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
