import pandas as pd
import sqlite3
import os

path_to_db = os.environ.get("RF_DB_DIR")

def query_rapidfire_db(query):
    con = sqlite3.connect(path_to_db)
    results = pd.read_sql_query(query, con)
    return results
